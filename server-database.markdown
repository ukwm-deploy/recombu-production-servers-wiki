# Database (database.ukwm.local)

# Copy ssh keys from admin for ukwm user - this allows it to clone the deployment tools

````
sudo apt-get update && sudo apt-get upgrade;
sudo apt-get install postgresql-9.4 postgresql-client-9.4 openjdk-8-jre nfs-kernel-server git mysql-server-5.6 mysql-client-5.6 monit mailutils php5 language-pack-en;

````

## Install composer

````
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin/
sudo mv /usr/local/bin/composer.phar /usr/local/bin/composer

````

## Deploy configs

````
sudo su - ukwm
cd ~;
mkdir deploy;
git clone git@bitbucket.org:ukwm/recombu-deploy-config.git deploy-config;
git clone git@bitbucket.org:ukwm/recombu-production-servers.git production-servers;

cd deploy-config;
git checkout master;
composer install;
sudo app/console deploy:config production <config>;

````

## Timezone

````

sudo dpkg-reconfigure --frontend noninteractive tzdata

````
## Add apt-key
````
NB dont forget to first log in to apt.ukwm.co.uk and add the *public IP* of the box to the nginx allow list... for the ubuntu_package_repository

sudo su -c 'wget -q -O - http://apt.ukwm.co.uk/ubuntu/gpg.key | apt-key add -';
sudo su -c 'echo "deb http://apt.ukwm.co.uk/ubuntu/ trusty-stable main" >>  /etc/apt/sources.list.d/ukwebmedia.list';

````
## Elastic search 

````
cd ~;
wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-0.90.3.deb;
sudo dpkg -i elasticsearch-0.90.3.deb;
sudo ln -s /usr/share/elasticsearch/bin/plugin /usr/local/bin/plugin;
````

### Diagnostics on elastic search

````
sudo plugin -install royrusso/elasticsearch-HQ
````

One can use http://localhost:9200/_plugin/HQ/ to access diagnostics


## Database configuration

````
sudo -u postgres createuser -s recombu -P;
sudo -u postgres createdb -E=UTF8 -O recombu recombu_cars;
````

### postgresql.conf

Change the listen_address value to match below:

````
listen_addresses = '!production_ip!'
````

### pg_hba.conf

Add the following line to the security table as bottom of file to allow access from the internal network

#### Staging:

````
host	all		all		192.168.16.0/24		md5

````

#### Production:

N.B. replace !production_ip! with the subnet containing the web boxes

````
host	all		all		!production_ip!/24		md5
````

## Configure run levels

````
sudo update-rc.d elasticsearch enable
sudo update-rc.d postgresql enable
````

## Configure CDN location

````
sudo mkdir -p /var/www/cdn;
sudo chown ukwm:www-data -R /var/www/;

````

### Commands
````
sudo update-rc.d nfs-kernel-server enable;
sudo service nfs-kernel-server start;
````

## Mysql

````
sudo mysql_install_db
sudo chown -R mysql: /var/lib/mysql
sudo service mysql restart

````

### configure the root password

````
sudo dpkg-reconfigure mysql-server-5.6
````

PW live: ^P[==Z5/x+zXa(J]

PW staging: suzuki



### Create database

In mysql:

````
create database recombu_mobile;

create database recombu_digital;

create database recombu_digital_v2;

create database recombu_mobile_feeds;

````


### Create users and grant perms

in mysql (staging(:

````
create user 'root'@'%' identified by 'suzuki';

grant all on *.* to 'root'@'%';

create user 'recombu'@'%' identified by 'suzuki';

grant all on recombu_mobile.* to 'recombu'@'%';

grant all on recombu_digital.* to 'recombu'@'%';

grant all on recombu_digital_v2.* to 'recombu'@'%';

grant all on recombu_mobile_feeds.* to 'recombu'@'%';

````

production:

````
create user 'root'@'%' identified by '^P[==Z5/x+zXa(J]';

grant all on *.* to 'root'@'%';

create user 'recombu'@'%' identified by '^P[==Z5/x+zXa(J]';

grant all on recombu_mobile.* to 'recombu'@'%';

grant all on recombu_digital.* to 'recombu'@'%';

grant all on recombu_digital_v2.* to 'recombu'@'%';

grant all on recombu_mobile_feeds.* to 'recombu'@'%';

````

### Import data from backups:
#### mysql

````
mysql -u username -p <DATA-BASE-NAME> < <filepath>
````

#### pgsql
````
sudo -u postgres psql -d <database> -f <filepath>
````