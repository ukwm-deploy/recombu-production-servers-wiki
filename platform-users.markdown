## Recombu Cloud Setup

## Generic Setup

````
sudo su -;
````


# Create .ssh skel

````
cd /etc/skel/
mkdir .ssh
chmod 0700 .ssh
touch .ssh/authorized_keys
chmod 0600 .ssh/authorized_keys
````

# Create users with unusable password

````
useradd -m -s /bin/bash --uid 900 -G www-data ukwm
useradd -m -s /bin/bash dplatt
useradd -m -s /bin/bash ogerrard
useradd -m -s /bin/bash slowes
useradd -m -s /bin/bash awhite
useradd -m -s /bin/bash sking
useradd -m -s /bin/bash jbreckenridge
````
# Set default password (suzuki) to allow sudo/password to work

````
usermod -p \$6\$rIlgITye\$EDGVRbF5dXaDo4X6CjnN2cDfA3gJJ3SVEUhbv5786PfH7mZ5WceKLDMUJGhLldy3aYCuvJou8/D/0d4oQPqHV1 dplatt
usermod -p \$6\$rIlgITye\$EDGVRbF5dXaDo4X6CjnN2cDfA3gJJ3SVEUhbv5786PfH7mZ5WceKLDMUJGhLldy3aYCuvJou8/D/0d4oQPqHV1 ogerrard
usermod -p \$6\$rIlgITye\$EDGVRbF5dXaDo4X6CjnN2cDfA3gJJ3SVEUhbv5786PfH7mZ5WceKLDMUJGhLldy3aYCuvJou8/D/0d4oQPqHV1 slowes
usermod -p \$6\$rIlgITye\$EDGVRbF5dXaDo4X6CjnN2cDfA3gJJ3SVEUhbv5786PfH7mZ5WceKLDMUJGhLldy3aYCuvJou8/D/0d4oQPqHV1 awhite
usermod -p \$6\$rIlgITye\$EDGVRbF5dXaDo4X6CjnN2cDfA3gJJ3SVEUhbv5786PfH7mZ5WceKLDMUJGhLldy3aYCuvJou8/D/0d4oQPqHV1 sking
usermod -p \$6\$rIlgITye\$EDGVRbF5dXaDo4X6CjnN2cDfA3gJJ3SVEUhbv5786PfH7mZ5WceKLDMUJGhLldy3aYCuvJou8/D/0d4oQPqHV1 jbreckenridge
````

# Add Users to sudo group
````
usermod -a -G sudo dplatt
usermod -a -G sudo ogerrard
usermod -a -G sudo slowes
usermod -a -G sudo awhite
usermod -a -G sudo sking
usermod -a -G sudo jbreckenridge
````

# Add public keys to /home/*/.ssh/authorized_keys

````
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDOArzDDZ83xwqlEv/HcbxH2fQ7b0mYNqVvIRfiBR1vzJJpi/zrjuoQWdnDrN55kiNEP7tA92liMcBKRuCHZOu+Ue+l90NYrkqc7ZKvdgR1NVeCZzIITtpQr0EhAepDac7dt0K7XjMReCI0SS95cBwADmNiBxrhRVNNekZip+KGtnZdI8D6McqEWVa2QxitpvWLs6DTmR6crvwmXa6wnHAcdZ59O+ChktE02NWNlYjUZp2KsUGdTn+MCqW84GBfm8aCq5BpJAgECSxii27B1XW4olgR3nNOgL0XGtwgY9uXKSijxD2ZYXL3dUjeffq+2YE9Y+yN8DSK7LNHD7KrICX3 dp@ukwm150.ukwm.local" > /home/dplatt/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAoCnR3ZpTr/bY7lJu3B3uFhIcU1PPT7D3vc5KE2pvWabExqwjlqOWcLiRIO6WlAf6Gdw1RbmLLABw2oFR4uqOlCDh8HxFgQ7vk0w/nfDFKbY2ZpBJ3dApnYNPRCUXuAxsplXl1XH4/DyVKww8M9ZY/Trb05Kuc5DvhDbvnHHaOB/8VVHgiEOS5UsCZHRiKEWQXBKRFvm4jD5bSRlI5UHu2lFDicKWCMjc0FXdRJdTiBriPZdc/3Nzn9LK5/mge9JiFYkShu3G9a46u5HnfG8EgWhp5JDccfsR8BM2mXrIgyCpBuJSmELkRmnqXahkgNCSrzqAHI58F2qhMWdBHlZTtQ== og@ukwm.local" > /home/ogerrard/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAsirGRTSx7tkGhktYGYAoE7puZ6xUAgrQA9Qe6n4HAjxAOiEabVvJODUc3opaUk7lAq2O9DgCaA/0aDuyrdcjIZkpOIL7K3PKisgd6VAc+DIjk13zpGVt87lXUSWKn6iAMTEHbECMYkayw8OR66UFRSbbQG5twvj2W66fFbHubhSKXBmtA1AdCdiT3nP8xaCQw3uLjr0m/DeBFU2QWVve73ruTw+yqHBF/Rj9tQTfoMqW0Xt4AgbbuG0rEoN0RWpoUusoqLr7srIaGs8HmZKcQ5V9UtmUR4DRmLE/l2qos4Rl0HELy5C9PT25yj9kuAgyfvM22RjkLKdx/lKqRHvoHQ== jbreckenridge@UKWM065" > /home/jbreckenridge/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDcuYqNK6uaI5Xm81+XK5p/3EsCaKafiHApg4tKJ5q7h55BoBSzjfr98SSZT1xUAKBZF3RkoW76Mdu2t4g8P6KDdpPR63phs2er3Sq12bkhJtiSapj4wb3TvFcHphT8JxHsAf9als6xrEP0zk82UGXkksMctUsQqeFzQn/5HET29sYFTkbMW4ka38L0I7Km9eiwO1YWPRwbeULU4wAed7Y0G3gkyK98URI4RZBPuB84Qnr7HVTZj3ua/6+LCY8+PMJ/SywuY8VlcU8jJGQj4qB0wGr0a6ojlVGcNsnC+y/WfFuaCpyzYRo0Fke5Pp08qrWhoIFgaIxnqbktDr65k6Ul slowes@ukwebmedia.com" > /home/slowes/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDjmoXOX+JUidNMJX1hl/cRUT9+HCM5IGxuVTGDzR0qEz8Vpq73vfEevcm6yQNng54e8csVhUh2cIp0a7MkJYAnbJKk3l3J2ZddI/Kn105TK852yweJCTPxS+PjbXPrNSPYHfWlpMeuEazzGmb83YkQi2JDDlOGYJOZmRwrqTxBdF0LylHx9Qa/0lC/cn8FEmQlCIJL/kBVqRUzq1qZXH/9cx2b1d/oU8YKA1mQEuF44UBBt+eEfzT1ohk7wjci4DMl6VfEnryb8SxBPRRtGWrYKQ5gbVTvPzlTWnBaaBTNrrehPq+ZBL+3LzXZZrCmwsD33V0UOfXJFsKqr2bUae6/ sking@ukwm110.ukwm.local" > /home/sking/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHSqror/qbHO3JPv08/Jkh0dY6vFJeteaRbrmmoctXlkP3D/kePr+zoPqwlYrqXcSQ1scRm3rpPGgIpE+6d+30yhA9F7j4cdVBwg2zAE2EtBnmumcnbI+U64k9eVg1g31bwujh5WMzKw3TsWCkHFfukiwZGe+Z42Gt2lN5mI+JEpqM6EORGijsLtGmsNeZ+V11z0BDFxqmjjVxlK5L5Ofg388OFvGWcWh7jJmdT5l4TSuqpSrQpVuXeTMTFLtsqkSkxlAeCa2x1u4s8g4974BtMCj9gG+S7eYio2cMLdodYUF7qUzIdXBHL1gLIonhNa/IjDa658hcNXCpMOVVTF87 adam@Adams-Mac-Pro.local" > /home/awhite/.ssh/authorized_keys
````

# Modify /etc/ssh/sshd_config

````
nano /etc/ssh/sshd_config
````
change/set these to:

```
PasswordAuthentication no
PermitRootLogin no
UseDNS no
```

and add this to the bottom of the file:

```
Match address 192.168.3.0/24
    PasswordAuthentication yes
```

staging:
```
Match address 192.168.16.0/24
    PasswordAuthentication yes
```

# Firewall - iptables - general

```
iptables-restore
*filter
:INPUT ACCEPT [1287:189799]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -i lo -j ACCEPT
-A INPUT -i eth1 -j ACCEPT
-A INPUT -i eth2 -j ACCEPT
-A INPUT -i eth0 -s 80.71.18.161/32 -p tcp -m tcp --dport 22 -j ACCEPT
-A INPUT -i eth0 -s 80.71.18.170/32 -p tcp -m tcp --dport 22 -j ACCEPT
-A INPUT -i eth0 -p icmp -m icmp --icmp-type 8 -j ACCEPT
-A OUTPUT -j ACCEPT
-A FORWARD -i eth0 -o eth1 -j DROP
-A INPUT -i eth0 -m state --state NEW -j DROP
COMMIT
ctrl-d
```

# Firewall - iptables - admin
```
iptables-restore
*filter
:INPUT ACCEPT [1287:189799]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -i lo -j ACCEPT
-A INPUT -i eth1 -j ACCEPT
-A INPUT -i eth2 -j ACCEPT
-A INPUT -i eth0 -p tcp -m tcp --dport 80 -j ACCEPT
-A INPUT -i eth0 -p tcp -m tcp --dport 443 -j ACCEPT
-A INPUT -i eth0 -s 80.71.18.161/32 -p tcp -m tcp --dport 22 -j ACCEPT
-A INPUT -i eth0 -s 80.71.18.170/32 -p tcp -m tcp --dport 22 -j ACCEPT
-A INPUT -i eth0 -p icmp -m icmp --icmp-type 8 -j ACCEPT
-A OUTPUT -j ACCEPT
-A FORWARD -i eth0 -o eth1 -j DROP
-A INPUT -i eth0 -m state --state NEW -j DROP
COMMIT
ctrl-d
```

At this point rebooting will lose these changes.
To save them:

```
apt-get install iptables-persistent
```

# Add UKWM Repo
```
wget -q -O - http://apt.ukwm.co.uk/ubuntu/gpg.key | apt-key add -
echo 'deb http://apt.ukwm.co.uk/ubuntu/ trusty-stable main' >>  /etc/apt/sources.list.d/ukwebmedia.list;
```

# !!ONLY RUN IF CLONING THE BOX!! Regenerate SSH Host Keys
````
rm /etc/ssh/ssh_host_*
dpkg-reconfigure openssh-server
````

# build-new-server.sh - linked in /etc/rc.local
````
#!/bin/bash

if ! [ -f /root/run_next_reboot ]
then
  exit
fi

rm /etc/ssh/ssh_host_*
dpkg-reconfigure openssh-server

 # rackspace-monitoring-agent --setup --username ukwebmedia --apikey a831aa2a810fbad76a5fede4784c0fe6

rm /root/.bash_history
rm /home/dplatt/.bash_history

rm /root/run_next_reboot
````

# Rackspace - High Performance servers. NB newer images may not require these steps!! check df -h to see if 1mb tmp parition is there - yes - then do these steps
new server now have /tmp mounted as 1MB.

```bash
fdisk -l
mkfs.ext3 /dev/xvde1 # double check correct drive.. should be about 40GB rackspace data disk.
mkdir /usr/local2
mount /dev/xvde1 /usr/local2
cp -pr /usr/local/* /usr/local2/
umount /usr/local2
mv /usr/local/* /usr/local2/
mount /dev/xvde1 /usr/local
mkdir /usr/local/tmp
chmod 1777 /usr/local/tmp
```

edit fstab and add:

```
/dev/xvde1      /usr/local      ext3    errors=remount-ro,noatime,barrier=0 0       1
```

add this to the bottom of /etc/apparmor.d/abstractions/user-tmp

```
  owner /usr/local/tmp/** rwkl,
  /usr/local/tmp/         rw,
```
