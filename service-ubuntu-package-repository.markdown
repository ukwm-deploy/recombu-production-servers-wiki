# Ubuntu package repo (apt.ukwm.co.uk)

### /etc/nginx/sites-enabled/ubuntu_package_repository

````
server {
  listen 80;
  server_name apt.ukwm.co.uk;

  access_log /var/log/nginx/apt_repository.log;
  error_log /var/log/nginx/apt_repository.log;

  location / {
    allow 162.13.154.145;
    allow 162.13.145.83;
    allow 162.13.15.216;
    allow 162.13.113.90;
    allow 162.13.14.63;
    allow 162.13.162.241;
    allow 162.13.115.223;
    allow 192.168.16.0/24;
    allow 192.168.17.0/24;
    deny all;

    root /var/packages;
    index index.html;
  }
}
````


### Set up ubuntu repository
````
sudo su;

apt-get install ruby gem rng-tools;

cd /etc/nginx/sites-enabled;

ln -s ../sites-available/ubuntu_package_repository;

gem install prm;

mkdir /var/packages/ubuntu;

chown -R ukwm:ukwm /var/packages/ubuntu;

````
### /etc/default/rng-tools

````
# Configuration for the rng-tools initscript
# $Id: rng-tools.default,v 1.1.2.5 2008-06-10 19:51:37 hmh Exp $

# This is a POSIX shell fragment

# Set to the input source for random data, leave undefined
# for the initscript to attempt auto-detection.  Set to /dev/null
# for the viapadlock and tpm drivers.
#HRNGDEVICE=/dev/hwrng
#HRNGDEVICE=/dev/null
HRNGDEVICE=/dev/urandom

# Additional options to send to rngd. See the rngd(8) manpage for
# more information.  Do not specify -r/--rng-device here, use
# HRNGDEVICE for that instead.
#RNGDOPTIONS="--hrng=intelfwh --fill-watermark=90% --feed-interval=1"
#RNGDOPTIONS="--hrng=viakernel --fill-watermark=90% --feed-interval=1"
#RNGDOPTIONS="--hrng=viapadlock --fill-watermark=90% --feed-interval=1"
#RNGDOPTIONS="--hrng=tpm --fill-watermark=90% --feed-interval=1"
````

## GPG key details

Type: 1
Size: 2048
Expiry: 0
Name: ukwebmedia
Email: tech@ukwebmedia.co.uk
No passphrase

````
sudo service rng-tools start;

gpg --gen-key;

````

## Set up import script for new bulids

/usr/local/bin/import_recent_builds

````
#!/bin/bash
for STABILITY in stable unstable
do

EXISTING=`find /var/packages/ubuntu/dists/precise-${STABILITY} -path *.deb | grep -v md5 | uniq`;
INCOMING=`find ~/repositories/incoming/$STABILITY -path *.deb | grep deb | uniq`;
for f in $INCOMING
do

BASENAME=`basename $f`;
if [ "`echo $EXISTING |  grep $BASENAME`" = "" ];then
    echo "Moving $f (${STABILITY})"
    mv $f ~/repositories/processing/$STABILITY;
    else
       echo "Not moving $f (${STABILITY})";
fi

done

prm -k 'Ukwebmedia' -t deb -p /var/packages/ubuntu -c main -r precise-$STABILITY -a amd64,i386 --directory ~/repositories/processing/$STABILITY; 

done

```` 

## PRM

### /etc/apt/sources.list.d/ukwebmedia.list

````
deb http://apt.ukwm.co.uk/ubuntu/ precise stable

````

### Set up repository

````
prm -t deb -p /var/packages/ubuntu -c stable,unstable -r xenial -a amd64,i386 -g;

mkdir -p /home/ukwm/repositories/incoming/stable;
mkdir -p /home/ukwm/repositories/incoming/unstable;

````

## Set NFS share of incoming

````
sudo apt-get install nfs-kernel-server;

````

### Append to /etc/default/nfs-common
````
NEED_IDMAPD=YES
````

### /etc/exports

````
# /etc/exports: the access control list for filesystems which may be exported
#		to NFS clients.  See exports(5).
#
# Example for NFSv2 and NFSv3:
# /srv/homes       hostname1(rw,sync,no_subtree_check) hostname2(ro,sync,no_subtree_check)
#
# Example for NFSv4:
# /srv/nfs4        gss/krb5i(rw,sync,fsid=0,crossmnt,no_subtree_check)
# /srv/nfs4/homes  gss/krb5i(rw,sync,no_subtree_check)
#

/home/ukwm/repositories/incoming  ci.ukwm.local(rw,sync,no_root_squash,no_subtree_check)

````

### Start servers

````
sudo update-rc.d nfs-kernel-server enable;
sudo service nfs-kernel-server start;

````

### Generate public gpg key

N.B Get the key from the 2nd part of the key listing of gpg below and include in subsequent command

````
gpg --list-keys;
gpg -a --output ~/gpg_id.pub --export <keyfile>;
sudo apt-key add ~/gpg_id.pub;

````

### Add the key to client machines

````

sudo su -c 'wget -q -O - http://apt.ukwm.co.uk/ubuntu/gpg.key | apt-key add -';
sudo su -c 'echo "deb http://apt.ukwm.co.uk/ubuntu/ precise stable" >>  /etc/apt/sources.list.d/ukwebmedia.list';

````