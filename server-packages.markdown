> Definition of packages for servers

### Overall platform
> Ubuntu with modern packages provided by PPAs

* `Ubuntu` 12.04 LTS [info](http://www.ubuntu.com/)


### Webserver
> Web applications

* `nginx` [info](http://wiki.nginx.org/)
* `PHP` 5.4+ [info](http://php.net/)
* `varnish` [info](https://www.varnish-cache.org/)

### DB / Data
> CDN, Databases, Elastic Search

* `mysql` 5.5+ [info](http://www.mysql.com/)
* `PostgreSQL` 9.1+ [info](http://www.postgresql.org/)
* `elasticsearch` 0.90.2+ [info](http://www.elasticsearch.org/)

### Admin
> Admin console for mobile feeds, publish for mobile and digital

* `ØMQ` (`zeromq`) [info](http://zeromq.org/)
* `PHP` 5.4+ [info](http://php.net/)
* `nginx` [info](http://wiki.nginx.org/)
* `memcached` [info](http://memcached.org/)