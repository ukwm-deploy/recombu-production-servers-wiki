127.0.1.1       web3.production.recombu.com web3
127.0.0.1       cdn
10.190.254.33   internal-webservice
127.0.0.1       recombu-mobile
127.0.0.1       recombu-digital
127.0.0.1       recombu
127.0.0.1 localhost

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

2a00:1a48:7809:102:be76:4eff:fe09:a9a   web3
192.168.3.4     web3
10.182.6.78     web3
162.13.162.241  web3

192.168.3.6     db1.private
192.168.3.5     admin1.private
192.168.3.5     mobile-feeds.recombu.com


## Add apt-key

### Production

````

sudo su -c 'wget -q -O - http://apt.ukwm.co.uk/ubuntu/gpg.key | apt-key add -';
sudo su -c 'echo "deb http://apt.ukwm.co.uk/ubuntu/ precise-stable-ssd main" >>  /etc/apt/sources.list.d/ukwebmedia.list';
sudo su -c 'echo "deb http://apt.ukwm.co.uk/ubuntu/ precise-stable main" >>  /etc/apt/sources.list.d/ukwebmedia.list';

````

### Staging

````

sudo su -c 'wget -q -O - http://apt.ukwm.co.uk/ubuntu/gpg.key | apt-key add -';
sudo su -c 'echo "deb http://apt.ukwm.co.uk/ubuntu/ precise-stable-ssd main" >>  /etc/apt/sources.list.d/ukwebmedia.list';
sudo su -c 'echo "deb http://apt.ukwm.co.uk/ubuntu/ precise-unstable" >>  /etc/apt/sources.list.d/ukwebmedia.list';

````

## Install latest build of varnish
Follow ubuntu instructions:

https://www.varnish-cache.org/installation/ubuntu


## Packages

Cause php 5.5 to be installed

````
sudo apt-get update;
sudo apt-get install python-software-properties;

````

````
sudo apt-get install php5-cli php5-fpm php5 php5-mysql php5-pgsql php5-mcrypt php5-sqlite php5-memcache php5-memcached php5-intl php5-curl php5-gd php5-imagick php-apc php-pear nginx-full git openjdk-7-jre-headless acl mysql-client memcached monit mailutils nfs-common recombu-frontend
````


## Timezone

````

sudo dpkg-reconfigure --frontend noninteractive tzdata

````

## Composer

````
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin/
sudo mv /usr/local/bin/composer.phar /usr/local/bin/composer
````

## CDN

### Setup NFS client

NB make sure to update idmap.conf with the following:

````

````

````
sudo mkdir /mnt/cdn;

````

#### Append to /etc/fstab
````
db1.private:/var/www/cdn /mnt/cdn nfs rsize=8192,wsize=8192,timeo=14,intr,rw

````

````
sudo mount -a
````

## Symlink assets (dont think any longer required)

#### Portal

````
cd /mnt/cdn;

sudo -u ukwm ln -sf /var/www/recombu/frontends/portal/web/css;
sudo -u ukwm ln -sf /var/www/recombu/frontends/portal/web/js;
sudo -u ukwm ln -sf /var/www/recombu/frontends/portal/web/bundles;

````

#### Cars

````
cd /mnt/cdn/cars;

sudo -u ukwm ln -sf /var/www/recombu/frontends/cars/web/css;
sudo -u ukwm ln -sf /var/www/recombu/frontends/cars/web/js;
sudo -u ukwm ln -sf /var/www/recombu/frontends/cars/web/bundles;

````

#### Mobile

````
cd /mnt/cdn/mobile;

sudo -u ukwm ln -sf /var/www/recombu/frontends/mobile/img;
sudo -u ukwm ln -sf /var/www/recombu/frontends/mobile/images;
sudo -u ukwm ln -sf /var/www/recombu/frontends/mobile/css;
sudo -u ukwm ln -sf /var/www/recombu/frontends/mobile/lib;

cd /var/www/recombu/frontends/mobile/images;

sudo -u ukwm ln -sf /mnt/cdn/mobile/models;
sudo -u ukwm ln -sf /mnt/cdn/mobile/news;
sudo -u ukwm ln -sf /mnt/cdn/mobile/phones;

````

#### Digital

````
cd /mnt/cdn/digital;

sudo -u ukwm ln -sf /var/www/recombu/frontends/digital/img;
sudo -u ukwm ln -sf /var/www/recombu/frontends/digital/images;
sudo -u ukwm ln -sf /var/www/recombu/frontends/digital/css;

cd /var/www/recombu/frontends/digital/images;

sudo -u ukwm ln -sf /mnt/cdn/digital/merchants;
sudo -u ukwm ln -sf /mnt/cdn/digital/news;

````

## Install recombu mobile and digital

````

sudo su - ukwm

cd /var/www/recombu/frontends/;

git clone git@bitbucket.org:ukwm/recombu-mobile.git mobile

cd mobile;

composer install;

cd  images;
ln -s /mnt/cdn/mobile/models;
ln -s /mnt/cdn/mobile/news;
ln -s /mnt/cdn/mobile/phones;

cd ../..;

git clone git@bitbucket.org:ukwm/recombu-digital.git digital

cd digital;

composer install;

cd images;
ln -s /mnt/cdn/digital/merchants;
ln -s /mnt/cdn/digital/news;

cd ../..;

git clone git@bitbucket.org:ukwm/recombu-compare-the-market.git mobile-compare-the-market

cd mobile-compare-the-market;

sudo php5enmod mcrypt;
composer install;
mkdir tmp;
chmod 0775 tmp;
mkdir logs cache;
sudo chown www-data:www-data cache/ logs/

cd ..;

git clone git@bitbucket.org:ukwm/recombu-assets.git assets;

cd assets/;

bin/vendors install

chmod 0775 -R ./web/assets;

````
exit;



