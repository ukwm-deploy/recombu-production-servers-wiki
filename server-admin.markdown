# Server Admin

#### Ensure the following entries are in hosts (make sure ips are approp)
````

127.0.0.1       localhost
127.0.0.1       gearbox-exchange.private
127.0.1.1       admin1.recombu.ukwm.local       admin1.recombu
127.0.0.1       internal-webservice
127.0.0.1       mobile-publish
192.168.3.6     db1.private

````

#### Append to /etc/fstab

````
db1.private:/var/www/cdn /mnt/cdn nfs rsize=8192,wsize=8192,timeo=14,intr,rw

````

````
sudo apt-get install nfs-common
sudo mkdir /mnt/cdn
mount -a

````

## Add apt-key

### Production

````

sudo su -c 'wget -q -O - http://apt.ukwm.co.uk/ubuntu/gpg.key | apt-key add -';
sudo su -c 'echo "deb http://apt.ukwm.co.uk/ubuntu/ precise-stable-ssd main" >>  /etc/apt/sources.list.d/ukwebmedia.list';
sudo su -c 'echo "deb http://apt.ukwm.co.uk/ubuntu/ precise-stable main" >>  /etc/apt/sources.list.d/ukwebmedia.list';

````

### Staging

````

sudo su -c 'wget -q -O - http://apt.ukwm.co.uk/ubuntu/gpg.key | apt-key add -';
sudo su -c 'echo "deb http://apt.ukwm.co.uk/ubuntu/ precise-stable-ssd main" >>  /etc/apt/sources.list.d/ukwebmedia.list';
sudo su -c 'echo "deb http://apt.ukwm.co.uk/ubuntu/ precise unstable" >>  /etc/apt/sources.list.d/ukwebmedia.list';

````
## Packages

Cause php 5.5 to be installed

````
sudo apt-get update;
sudo apt-get install python-software-properties;
sudo add-apt-repository ppa:ondrej/php5;

````

````

cd ~;
mkdir deploy;
git clone https://vcs.ukwm.co.uk/Recombu/production-servers.git;
git clone https://vcs.ukwm.co.uk/Recombu/deploy_config.git;

cd deploy_config;
sudo app/console deploy:config production admin;

````

````
sudo apt-get update;
sudo apt-get install php5-cli php5-fpm php5 php5-mcrypt php5-mysql php5-pgsql php5-sqlite php5-memcached php5-memcache php5-intl php5-curl php5-gd php5-imagick php-apc php-pear nginx-full git openjdk-7-jre-headless compass-yui-plugin nfs-common imagemagick recombu-cars-admin php5-zmq recombu-iws memcached memcache monit mailutils postgresql-client-common

```

## Timezone

````

sudo dpkg-reconfigure --frontend noninteractive tzdata

````

### Pin repo - /etc/apt/preferences.d/ukwebmedia

````

Package: *
Pin: release n=precise-stable-ssd
Pin-Priority: 900

Package: *
Pin: release n=precise-stable
Pin-Priority: -10

````

## Install composer

````
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin/
sudo mv /usr/local/bin/composer.phar /usr/local/bin/composer

sudo mkdir /var/www/.composer
sudo chown www-data:www-data /var/www/.composer/
````

## Gearbox server

````
cd /usr/local/
sudo mkdir workers

sudo chown -R ukwm:www-data ./workers/
sudo chown -R ukwm:www-data ./workers/
sudo chmod 0775 -R workers/

cd /usr/local/workers;

sudo -u ukwm git clone https://vcs.ukwm.co.uk/Services/Server.git server;

cd server;

sudo -Hu ukwm composer install;

````

/usr/local/workers/server/app/config/servers.xml

````
<?xml version="1.0" ?>
<servers xmlns="http://ukwm.co.uk/schema/gearbox/servers"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <server id="exchange" user="ukwm" group="www-data">
        <frontend>tcp://127.0.0.1:5570</frontend>
        <backend>tcp://127.0.0.1:5571</backend>
    </server>
</servers>

````


## Worker scripts

````


cd /usr/local/workers;

sudo -u ukwm git clone https://vcs.ukwm.co.uk/Services/CAPImageDownload.git cap-image-download;
cd cap-image-download;
sudo -Hu composer install;

cd ..;
git clone https://vcs.ukwm.co.uk/Services/Resize.git resize;
cd resize;
composer install;

cd ..;
sudo -u ukwm git clone https://vcs.ukwm.co.uk/Services/search-indexer.git search-indexer
cd search-indexer;
sudo -Hu ukwm composer install;
echo 'iws_baseurl: http://internal-webservice:8080/api/v1' > app/config/parameters.yml;

exit;

````

### Cap Image download (app/config/worker.xml)

````
<?xml version="1.0" ?>
<worker xmlns="http://ukwm.co.uk/schema/gearbox/servers"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <thread user="ukwm" group="www-data">
        <exchange>tcp://127.0.0.1:5571</exchange>
        <tag>capimages</tag>
    </thread>
</worker>
````

### Resize (app/config/worker.xml)

````
<?xml version="1.0" ?>
<worker xmlns="http://ukwm.co.uk/schema/gearbox/servers"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <thread user="ukwm" group="www-data">
        <exchange>tcp://127.0.0.1:5571</exchange>
        <exchange_frontend>tcp://127.0.0.1:5570</exchange_frontend>
        <tag>resize</tag>
    </thread>

</worker>
````

### Search Indexer  (app/config/worker.xml)

````
<?xml version="1.0" ?>
<worker xmlns="http://ukwm.co.uk/schema/gearbox/servers"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <thread user="ukwm" group="www-data">
        <exchange>tcp://127.0.0.1:5571</exchange>
        <tag>search</tag>
    </thread>
</worker>
````

## NGINX

### Set up autostart behaviour

````
sudo update-rc.d nginx enable;
sudo update-rc.d php5-fpm enable;

````

# Install mobile and digital as scripts

````
sudo mkdir -p /usr/local/scripts;

sudo chown -R ukwm:ukwm /usr/local/scripts;

cd /usr/local/scripts;

sudo -u ukwm git clone https://vcs.ukwm.co.uk/Recombu/mobile.git mobile;
cd mobile;

sudo -Hu ukwm composer install;

cd ..;
sudo -u ukwm git clone https://vcs.ukwm.co.uk/Recombu/digital.git digital;
cd digital;
sudo -Hu ukwm composer install


````

## Setup support files for mobile and digital
/home/ukwm/recombu_cron.sh

```bash
#!/bin/bash

#email='ukwm-crons@googlegroups.com'
email='rackspace-recombu@ukwebmedia.com'
date=`/bin/date +%Y%m%d`
hostname=`/bin/hostname`

touch /tmp/cronlog$date

echo 'UKWM Command - Cron Log' >> /tmp/cronlog$date
echo '=======================' >> /tmp/cronlog$date
echo 'Feed Import' >> /tmp/cronlog$date
cd /var/www/recombu/frontends/mobile-feeds
Console/cake feed -a

echo 'simonly feed importer' >> /tmp/cronlog$date
echo 'sim-only.php' >> /tmp/cronlog$date
cd /var/www/recombu/frontends/mobile-admin
/usr/bin/php sim-only.php >> /tmp/cronlog$date

echo '_tools' >> /tmp/cronlog$date
cd /usr/local/scripts/mobile/_tools/
/usr/bin/php -q update_price_from.php >> /tmp/cronlog$date
/usr/bin/php -q rebuild-ranks.php >> /tmp/cronlog$date

echo '_tools' >> /tmp/cronlog$date
cd /usr/local/scripts/mobile/_tools/
/usr/bin/php -q auto_popular_models.php standard >> /tmp/cronlog$date
/usr/bin/php -q auto_popular_models.php deals >> /tmp/cronlog$date
/usr/bin/php -q auto_popular_news.php >> /tmp/cronlog$date
/usr/bin/php -q auto_popular_news.php apps >> /tmp/cronlog$date
/usr/bin/php -q auto_popular_editorial.php >> /tmp/cronlog$date
/usr/bin/php -q generate_similar_phones.php >> /tmp/cronlog$date
/usr/bin/php -q relate_content.php >> /tmp/cronlog$date
/usr/bin/php -q identify_auto_tags.php >> /tmp/cronlog$date

echo 'feeds' >> /tmp/cronlog$date
cd /usr/local/scripts/mobile/_tools/feeds/
/usr/bin/php -q -d register_argc_argv=On render_deal_feeds.php --silent >> /tmp/cronlog$date

echo 'email setup' >> /tmp/cronlog$date
mail $email -s"Cron Notification for $hostname" < /tmp/cronlog$date

# clean up otherwise loads of files in tmp (BAD!)
rm -rf /tmp/cronlog$date
```

Crons

```bash
# PUBLISH
* * * * * cd /usr/local/scripts/mobile/_tools && /usr/bin/php -q push_reviews_to_news.php
# */2 * * * * cd /var/www/recombu/publish/mobile/_tools && /usr/bin/php -q tweet_articles.php > /dev/null # going to tweet via hootsuite (9/5/12 OG)
#*/5 * * * * /usr/bin/php /usr/local/scripts/mobile/_tools/get_twitter_follower_count.php
*/5 * * * * flock -n /tmp/cache_partner_config.lock /usr/bin/php /usr/local/scripts/mobile/_tools/cache_partner_config.php
# * * * * * cd /var/www/recombu/publish/mobile/_tools && /usr/bin/php -q generate_bitly_for_news.php > /dev/null 2>&1 # going to tweet via hootsuite instead (9/5/12 OG)
# */2 * * * * /usr/bin/php /usr/local/scripts/mobile/_tools/tweet_reviews.php # disabled HW 16/4/2012 (not tweeting as missing bitly)

# ADMIN
30 1 * * 1-5 /bin/sh /home/ukwm/recombu_cron.sh
#30 9,13 * * * cd /var/www/recombu/frontends/mobile-admin && /usr/bin/php process-tesco.php
0 8 * * * cd /var/www/recombu/frontends/mobile-admin && /usr/bin/php process-affordable.php
0 7 * * * cd /var/www/recombu/frontends/mobile-admin && /usr/bin/php process-three.php
*/15 * * * * cd /var/www/recombu/frontends/mobile-admin/_tools && /usr/bin/php disable_expired_exclusives.php
*/15 * * * * cd /var/www/recombu/frontends/mobile-admin/_tools && /usr/bin/php update_cashback_deal_visibility.php
0 * * * * cd /var/www/recombu/frontends/mobile-admin && /usr/bin/php youtube.php > /dev/null
10 * * * * cd /var/www/recombu/frontends/mobile-admin && /usr/bin/php youtube.php --digital > /dev/null

# DIGITAL
*/15 * * * * cd /usr/local/scripts/digital/_tools && /usr/bin/php get_bbf_package_data.php
#*/15 * * * * cd /usr/local/scripts/digital/_tools && /usr/bin/php get_bbf_provider_data.php
#*/5 * * * * cd /usr/local/scripts/digital/_tools && /usr/bin/php get_twitter_follower_count.php
0 22 * * * cd /usr/local/scripts/digital/_tools && /usr/bin/php update_provider_speeds.php
0 9 * * 1-5 cd /usr/local/scripts/digital/_tools && /usr/bin/php report_bbf_package_data.php
30 1 * * * cd /usr/local/scripts/digital/_tools && /usr/bin/php auto_popular_editorial.php
30 1 * * * cd /usr/local/scripts/digital/_tools && /usr/bin/php auto_popular_news.php

# MOBILE
0 * * * * cd /usr/local/scripts/mobile/_tools && /usr/bin/php generate_news_sitemap.php
*/15 * * * * cd /usr/local/scripts/mobile/_tools && /usr/bin/php expire_partner_cache.php
*/30 * * * * cd /usr/local/scripts/mobile/_tools && /usr/bin/php cache_partner_config.php

#CARS
*/5 *  *   *   *     php /var/www/recombu/services/internal-webservice/app/console document:scheduler:publish --env=prod
```


## Install cars and mobile-admin

````
cd /var/www/recombu/frontends;

sudo -u ukwm git clone https://vcs.ukwm.co.uk/Recombu/mobile-admin.git mobile-admin;

sudo -u ukwm git clone https://vcs.ukwm.co.uk/Recombu/digital-publish.git digital-publish;

sudo -u ukwm git clone https://vcs.ukwm.co.uk/Recombu/mobile-publish.git mobile-publish;

sudo -u ukwm git clone https://vcs.ukwm.co.uk/Recombu/mobile-feeds.git mobile-feeds;

sudo ln -sf /etc/nginx/sites-available/mobile-admin /etc/nginx/sites-enabled/mobile-admin;

sudo ln -sf /etc/nginx/sites-available/mobile-publish /etc/nginx/sites-enabled/mobile-publish;

sudo ln -sf /etc/nginx/sites-available/mobile-feeds /etc/nginx/sites-enabled/mobile-feeds;

sudo ln -sf /etc/nginx/sites-available/digital-publish /etc/nginx/sites-enabled/digital-publish;

sudo ln -sf /etc/nginx/sites-available/cars-admin /etc/nginx/sites-enabled/cars-admin;

sudo ln -sf /etc/nginx/sites-available/internal-webservice /etc/nginx/sites-enabled/internal-webservice;

echo 'recombu.publish:$apr1$ASZC1QN7$LK3mfYPNGoDSoa2HvRUEW.' | sudo tee -a /var/www/recombu/frontends/mobile-admin/.htpasswd;

````

Copy the .htpasswd file into mobile-admin from one of the existing configs