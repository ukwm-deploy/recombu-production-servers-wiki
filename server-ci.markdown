## Install packages and prepare folders

````
sudo apt-get install nfs-common ruby gem php-pear phpunit git compass-yui-plugin php;

mkdir -p ~/build/stable;
mkdir -p ~/build/unstable;

sudo mkdir -p /mnt/outgoing;

````

## Install elastic search

````
sudo wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -;
echo "deb https://packages.elastic.co/elasticsearch/2.x/debian stable main" | sudo tee -a /etc/apt/sources.list.d/elasticsearch-2.x.list;
sudo apt-get update && sudo apt-get install elasticsearch;
sudo /bin/systemctl daemon-reload;
sudo /bin/systemctl enable elasticsearch.service;

````

## Append to /etc/fstab

````
apt.ukwm.co.uk:/home/ukwm/incoming /mnt/outgoing nfs rsize=8192,wsize=8192,timeo=14,intr

````

### Mount the incoming folder

````
sudo mount -a;

````

### Install Jenkins

Execute in shell

````
wget -q -O - http://pkg.jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -;
echo "deb http://pkg.jenkins-ci.org/debian binary/" >> /etc/apt/sources.list;

sudo apt-get update
sudo apt-get install jenkins

````

### Jenkins initial setup checklist

## Configure sudo

````
jenkins  ALL=(ukwm) /bin/mv /usr/local/build/* /mnt/outgoing/*

````

## Install fpm (Ruby Gem) and prepare root build directories

````
sudo gem install fpm;

sudo mkdir -p /usr/local/build/stable;

sudo chown -R recombu:recombu /usr/local/build;

sudo chmod 0777 -R /usr/local/build;

````

## Install Phing, composer and phpunit

````
sudo pear channel-discover pear.phing.info
sudo pear install phing/phing

sudo pear config-set auto_discover 1
sudo pear install pear.phpunit.de/PHPUnit

curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin/
sudo -u jenkins mv /usr/local/bin/composer.phar /usr/local/bin/composer

````

## Add apt-key
````

sudo su -c 'wget -q -O - http://apt.ukwm.co.uk/ubuntu/gpg.key | apt-key add -';
sudo su -c 'echo "deb http://apt.ukwm.co.uk/ubuntu/ precise-unstable main" >  /etc/apt/sources.list.d/ukwebmedia.list';
sudo su -c 'echo "deb http://apt.ukwm.co.uk/ubuntu/ precise-stable main" >>  /etc/apt/sources.list.d/ukwebmedia.list';

````

## Configure git ssl

/etc/profile.d/git.sh

````
export GIT_SSL_NO_VERIFY=true;

````

## Set up apt repo

````
sudo -u ukwm prm -t deb -p /var/packages/ubuntu -r precise-stable -a i386,amd64 -c main;
sudo -u ukwm prm -t deb -p /var/packages/ubuntu -r precise-unstable -a i386,amd64 -c main;
sudo -u ukwm prm -t deb -p /var/packages/ubuntu -r trusty-stable -a i386,amd64 -c main;
sudo -u ukwm prm -t deb -p /var/packages/ubuntu -r trusty-unstable -a i386,amd64 -c main;

````